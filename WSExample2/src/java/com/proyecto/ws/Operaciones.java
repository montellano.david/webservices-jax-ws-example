/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.proyecto.ws;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author davidbm
 */
@WebService(serviceName = "Operaciones")
public class Operaciones {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }
    
    
    //Operacion suma
    @WebMethod(operationName = "suma")
    public double suma(int numeroA, int numeroB) {
        
        double resultado = numeroA + numeroB;        
        return resultado;
    }
    
    //Operacion resta
    @WebMethod(operationName = "resta")
    public double resta(int numeroA, int numeroB) {
        
        double resultado = numeroA - numeroB;        
        return resultado;
    }
    
     //Operacion multiplicacion
    @WebMethod(operationName = "multiplicacion")
    public double multiplicacion(int numeroA, int numeroB) {
        
        double resultado = numeroA * numeroB;        
        return resultado;
    }
    
     //Operacion division
    @WebMethod(operationName = "division")
    public double division(int numeroA, int numeroB) {
        
        double resultado = numeroA / numeroB;        
        return resultado;
    }
}
